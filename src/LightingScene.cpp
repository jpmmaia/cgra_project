#include "LightingScene.h"
#include "CGFaxis.h"
#include "CGFapplication.h"

#include <math.h>
#include <time.h>

using namespace std;

float pi = acos(-1.0);
float deg2rad = pi / 180.0;

#define BOARD_HEIGHT 6.0
#define BOARD_WIDTH 6.4

// Positions for two lights
float light0_pos[4] = { 4, 6.0, 1.0, 1.0 };
float light1_pos[4] = { 10.5, 6.0, 1.0, 1.0 };

float light2_pos[4] = { 10.5, 6.0, 5.0, 1.0 };
float light3_pos[4] = { 4, 6.0, 5.0, 1.0 };

// Global ambient light (do not confuse with ambient component of individual lights)
float globalAmbientLight[4] = { 0.0, 0.0, 0.0, 1.0 };

// number of divisions
#define BOARD_A_DIVISIONS 30
#define BOARD_B_DIVISIONS 100

// Coefficients for material A
float ambA[3] = { 0.2, 0.2, 0.2 };
float difA[3] = { 0.6, 0.6, 0.6 };
float specA[3] = { 0.0, 0.8, 0.8 };
float shininessA = 120.f;

// Coefficients for material B
float ambB[3] = { 0.2, 0.2, 0.2 };
float difB[3] = { 0.6, 0.6, 0.6 };
float specB[3] = { 0.8, 0.8, 0.8 };
float shininessB = 120.f;

float ambientNull[4] = { 0, 0, 0, 1 };
float yellow[4] = { 1, 1, 0, 1 };

void LightingScene::init()
{
	// Enables lighting computations
	glEnable(GL_LIGHTING);

	// Sets up some lighting parameters
	// Computes lighting only using the front face normals and materials
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);

	// Define ambient light (do not confuse with ambient component of individual lights)
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmbientLight);

	// Declares and enables three lights, with null ambient component

	light0 = new CGFlight(GL_LIGHT0, light0_pos);
	light0->setAmbient(ambientNull);
	light0->setSpecular(yellow);

	//light0->disable();
	light0->enable();

	light1 = new CGFlight(GL_LIGHT1, light1_pos);
	light1->setAmbient(ambientNull);

	//light1->disable();
	light1->enable();

	light2 = new CGFlight(GL_LIGHT2, light2_pos);
	light2->setAmbient(ambientNull);
	light2->setKc(0.0f);
	light2->setKl(1.0f);
	light2->setKq(0.0f);

	//light2->disable();
	light2->enable();

	light3 = new CGFlight(GL_LIGHT3, light3_pos);
	light3->setAmbient(ambientNull);
	light3->setSpecular(yellow);
	light3->setKc(0.0f);
	light3->setKl(0.0f);
	light3->setKq(0.2f);

	//light3->disable();
	light3->enable();

	float windowLightPos [] = { 0.2f, 5.0f, 7.5f, 1.0f };
	windowLight = new CGFlight(GL_LIGHT4, windowLightPos);
	windowLight->setAmbient(ambientNull);
	windowLight->enable();

	// Uncomment below to enable normalization of lighting normal vectors
	glEnable(GL_NORMALIZE);

	//Declares scene elements
	lamp = new MyLamp(12, 12);
	table = new MyTable();
	chair = new MyChair();
	planeWall = new Plane();
	floor = new Plane();
	boardA = new Plane(BOARD_A_DIVISIONS);
	boardB = new Plane(BOARD_B_DIVISIONS);
	collumn = new MyCylinder(12, 1, true);
	clock = new MyClock();
	paperPlane = new MyPaperPlane();
	robot = new MyRobot();
	leftWall = new MyWindowWall();
	clockUpdate = true;

	//Declares materials
	createMaterials();

	this->setUpdatePeriod(100);
}

void LightingScene::display()
{

	// ---- BEGIN Background, camera and axis setup

	// Clear image and depth buffer everytime we update the scene
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Initialize Model-View matrix as identity (no transformation
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Apply transformations corresponding to the camera position relative to the origin
	CGFscene::activeCamera->applyView();

	light0->draw();
	light1->draw();
	light2->draw();
	light3->draw();
	windowLight->draw();

	// Draw axis
	axis.draw();

	// ---- END Background, camera and axis setup

	// ---- BEGIN Primitive drawing section

	// Lamp
	glPushMatrix();
	glTranslatef(7.5f, 9.0f, 7.5f);
	lamp->draw();
	glPopMatrix();

	//First Table
	glPushMatrix();
	glTranslated(4.3, 0, 8);
	table->draw();

	// First Chair
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 1.5f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	chair->draw();
	glPopMatrix();
	glPopMatrix();

	//Second Table
	glPushMatrix();
	glTranslated(10.7, 0, 8);
	table->draw();

	// Second Chair
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 1.5f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	chair->draw();
	glPopMatrix();
	glPopMatrix();

	//Floor
	floorAppearance->apply();

	glPushMatrix();
	glTranslated(7.5, 0, 7.5);
	glScaled(15, 0.2, 15);
	floor->draw();
	glPopMatrix();

	//LeftWall
	leftWall->draw();

	//PlaneWall
	materialWall->apply();

	glPushMatrix();
	glTranslated(7.5, 5, 0);
	glRotated(90.0, 1, 0, 0);
	glScaled(15, 0.2, 10);
	planeWall->draw();
	glPopMatrix();


	// Board A
	glPushMatrix();
	glTranslated(4, 4, 0.2);
	glScaled(BOARD_WIDTH, BOARD_HEIGHT, 1);
	glRotated(90.0, 1, 0, 0);
	slidesAppereance->apply();
	boardA->draw();
	glPopMatrix();

	// Board B
	glPushMatrix();
	glTranslated(10.5, 4, 0.2);
	glScaled(BOARD_WIDTH, BOARD_HEIGHT, 1);
	glRotated(90.0, 1, 0, 0);
	boardAppeareance->apply();
	boardB->draw();
	glPopMatrix();

	// Clock
	glPushMatrix();
	glTranslatef(7.25f, 8.5f, 0.0f);
	clock->draw();
	glPopMatrix();

	// Paper Plane
	glPushMatrix();
	paperPlane->draw();
	glPopMatrix();

	// Collumns
	glPushMatrix();
	glTranslatef(2.0f, 0.0f, 12.0f);
	glScalef(1.0f, 10.0f, 1.0f);
	collumn->draw();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(13.0f, 0.0f, 12.0f);
	glScalef(1.0f, 10.0f, 1.0f);
	collumn->draw();
	glPopMatrix();

	// Robot
	robot->draw();

	// ---- END Primitive drawing section


	// We have been drawing in a memory area that is not visible - the back buffer, 
	// while the graphics card is showing the contents of another buffer - the front buffer
	// glutSwapBuffers() will swap pointers so that the back buffer becomes the front buffer and vice-versa
	glutSwapBuffers();
}

void LightingScene::update(unsigned long millis)
{
	if (clockUpdate)
		clock->update(millis);

	paperPlane->update(millis);
}

void LightingScene::createMaterials()
{
	slidesAppereance = new CGFappearance(ambA, difA, specA, shininessA);
	slidesAppereance->setTexture("slides.png");
	slidesAppereance->setTextureWrap(GL_CLAMP, GL_CLAMP);

	boardAppeareance = new CGFappearance(ambB, difB, specB, shininessB);
	boardAppeareance->setTexture("board.png");
	boardAppeareance->setTextureWrap(GL_CLAMP, GL_CLAMP);
	boardB->setTextureDimensions(512, 372);

	float ambWall [] = { 0.2f, 0.2f, 0.2f };
	float difWall [] = { 0.9f, 0.9f, 0.9f };
	float specWall [] = { 0.1f, 0.1f, 0.1f };
	float shininessWall = 20.0f;

	materialWall = new CGFappearance(ambWall, difWall, specWall, shininessWall);
	materialWall->setTexture("wall.png");

	float ambFloor [] = { 0.2f, 0.2f, 0.2f };
	float difFloor [] = { 0.4f, 0.0f, 0.0f };
	float specFloor [] = { 0.1f, 0.1f, 0.1f };
	float shininessFloor = 20.0f;

	floorAppearance = new CGFappearance(ambFloor, difFloor, specFloor, shininessFloor);
	floorAppearance->setTexture("floor.png");
	floorAppearance->setTextureWrap(GL_REPEAT, GL_REPEAT);
	floor->setTextureRepeat(10.0f, 12.0f);

	float ambCollumn[] = { 0.2f, 0.2f, 0.2f };
	float difCollumn[] = { 0.8f, 0.8f, 0.8f };
	float specCollumn[] = { 0.3f, 0.3f, 0.13 };
	float shininessCollumn = 40.0f;
	CGFappearance* sideAppearance = new CGFappearance(ambCollumn, difCollumn, specCollumn, shininessCollumn);
	sideAppearance->setTexture("whiteMarble.png");
	sideAppearance->setTextureWrap(GL_REPEAT, GL_REPEAT);

	collumn->setSideAppearance(sideAppearance);
}

LightingScene::~LightingScene()
{
	delete(light0);
	delete(light1);
	delete(light2);
	delete(light3);
	delete(windowLight);

	delete(lamp);
	delete(table);
	delete(chair);
	delete(planeWall);
	delete(leftWall);
	delete(boardA);
	delete(boardB);
	delete(floor);
	delete(collumn);
	delete(clock);
	delete(paperPlane);
	delete(robot);

	delete(slidesAppereance);
	delete(boardAppeareance);
	delete(materialWall);
	delete(floorAppearance);
}

MyRobot* LightingScene::getRobot() const
{
	return robot;
}

vector<CGFlight*> LightingScene::getLights() const
{
	vector<CGFlight*> lights(5, 0);
	lights[0] = light0;
	lights[1] = light1;
	lights[2] = light2;
	lights[3] = light3;
	lights[4] = windowLight;

	return lights;
}

void LightingScene::toggleClock(bool enable)
{
	if (enable)
	{
		clock->reinitialize();
		clockUpdate = true;
	}
	else
		clockUpdate = false;
}