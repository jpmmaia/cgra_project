#pragma once

#include "MyUnitCube.h"
#include "CGFappearance.h"

class MyChair : CGFobject
{
public:
	MyChair();
	~MyChair();

	void draw();
	void createMaterial();

private:
	MyUnitCube myUnitCube;
	float angle;
	float xShift;
	float zShift;

	CGFappearance* material;
};