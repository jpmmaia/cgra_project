#ifndef MY_LANDSCAPE_H
#define MY_LANDSCAPE_H

#include "Plane.h"
#include "Vector3f.h"

#include <vector>

class MyLandscape
{
public:
	MyLandscape();
	~MyLandscape();

	void draw();

private:
	void calculateVertices(unsigned int segments, unsigned int rings);
	void calculateIndices(unsigned int segments, unsigned int rings);
	void calculateNormals();
	void calculateTextels();

private:
	std::vector<GLfloat> mVertices;
	std::vector<GLushort> mIndices;
	std::vector<GLfloat> mNormals;
	std::vector<GLfloat> mTextels;

	Vector3f mPosition;
	Vector3f mRotation;
	Vector3f mScale;

	CGFappearance mMaterial;
};

#endif