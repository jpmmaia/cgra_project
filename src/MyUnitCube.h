#ifndef MYUNITCUBE_H
#define MYUNITCUBE_H

#include "CGFobject.h"

class MyUnitCube : CGFobject
{
public:
	void draw();

private:
	void drawRect();
};

#endif // !MYUNITCUBE_H