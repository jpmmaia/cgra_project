#pragma once

#include "CGFobject.h"
#include "Vector3f.h"

#include <vector>

class MyPaperPlane : CGFobject
{
public:
	MyPaperPlane();
	~MyPaperPlane();

	void draw();
	virtual void update(float millis);

	void setPosition(float x, float y, float z);
	void setRotation(float xAngle, float yAngle, float zAngle);
	void setDimensions(float width, float height, float depth);
	void setAcceleration(float x, float y, float z);

private:
	Vector3f rt(float t, const Vector3f& r, const Vector3f& r0) const;
	Vector3f vt(float t, const Vector3f& r) const;

private:
	std::vector<std::vector<GLfloat>> vertices;
	std::vector<std::vector<GLfloat>> normals;

	Vector3f position0;
	Vector3f position;
	Vector3f rotation;
	Vector3f rotation0;

	Vector3f dimensions; // Width, Height, Depth
	Vector3f acceleration;

	bool firstUpdate;
	float t0;
};