#pragma once

#include <CGFobject.h>
#include <vector>
#include <CGFappearance.h>

class MyLamp
{
public:
	MyLamp(int rings, int sectors);
	~MyLamp();

	void draw();

private:
	void createMaterial();

private:
	float theta;
	float phi;

	int stacks;
	int slices;

	CGFappearance* material;
};