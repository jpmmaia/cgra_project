#include "MyRobot.h"

#include <math.h>
#include <iostream>

using namespace std;

const float MyRobot::degree2rad = acos(-1.0f) / 180.0f;
const float MyRobot::rad2degree = 180.0f / acos(-1.0f);

MyRobot::MyRobot()
{
	setPosition(Vector3f(7.5f, 0.0f, 7.5f));
	setRotation(Vector3f(0.0f, -atan(7.3f / 3.5f) * rad2degree + 180.0f, 0.0f));

	initialize(8);
}

MyRobot::~MyRobot()
{
	for (unsigned int i = 0; i < materials.size(); i++)
	{
		if (materials[i])
		{
			delete materials[i];
			materials[i] = 0;
		}
	}
}

bool MyRobot::initialize(unsigned int stacks)
{
	if (stacks == 0)
		return false;

	this->stacks = stacks;
	this->currentTexture = 0;
	this->wireframeMode = false;

	calculateVertices(stacks);
	calculateIndices(stacks);
	calculateNormals(stacks);
	calculateTextels(stacks);
	createMaterial();

	return true;
}

void MyRobot::draw()
{
	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(rotation.z, 0.0f, 0.0f, 1.0f);

	if (wireframeMode)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		
	materials[currentTexture]->apply();

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	
	glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
	glNormalPointer(GL_FLOAT, 0, &normals[0]);
	glTexCoordPointer(2, GL_FLOAT, 0, &textels[0]);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, &indices[0]);
	
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	if (wireframeMode)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


	glPopMatrix();
}

void MyRobot::move(float value)
{
	Vector3f direction = Vector3f(cos(rotation.y * degree2rad), 0.0f, -sin(rotation.y * degree2rad));
	direction *= value;

	position += direction;
}

void MyRobot::addPosition(Vector3f position)
{
	this->position += position;
}
void MyRobot::setPosition(Vector3f position)
{
	this->position = position;
}

void MyRobot::addRotation(Vector3f rotation)
{
	this->rotation += rotation;
}
void MyRobot::setRotation(Vector3f rotation)
{
	this->rotation = rotation;
}

void MyRobot::setTexture(unsigned int index)
{
	currentTexture = index;
}

void MyRobot::setWireframeMode(bool enable)
{
	wireframeMode = enable;
}

void MyRobot::calculateVertices(unsigned int stacks)
{
	// Base
	float deltaP = 1.0f / 3.0f;
	GLfloat base [] =
	{
		0.5f, 0.0f, -0.5f,
		0.5f, 0.0f, -0.5f + deltaP,
		0.5f, 0.0f, 0.5f - deltaP,

		0.5f, 0.0f, 0.5f,
		0.5f - deltaP, 0.0f, 0.5f,
		-0.5f + deltaP, 0.0f, 0.5f,

		-0.5f, 0.0f, 0.5f,
		-0.5f, 0.0f, 0.5 - deltaP,
		-0.5f, 0.0f, -0.5f + deltaP,

		-0.5f, 0.0f, -0.5f,
		-0.5f + deltaP, 0.0f, -0.5f,
		0.5f - deltaP, 0.0f, -0.5f,
	};

	GLfloat top[36];
	for (unsigned int i = 0; i < 12; i++)
	{
		unsigned int index = i * 3;

		float angle = acos(-1) * (2.0f * i - 3.0f) / 12.0f;   
		top[index] = 0.25f * cos(angle);
		top[index + 1] = 1.0f;
		top[index + 2] = 0.25f * sin(angle);
	}

	const unsigned int nVertices = 36 * (stacks + 1);
	vertices = vector<GLfloat>(nVertices);
	for (unsigned int i = 0; i < 36; i++)
	{
		vertices[i] = base[i];
		vertices[nVertices - 36 + i] = top[i];
	}
	
	if (stacks == 1)
		return;

	// Middle vertices
	vector<Vector3f> vectors = vector<Vector3f>(12);
	for (unsigned int i = 0; i < vectors.size(); i++)
	{
		unsigned int index = i * 3;
		Vector3f p1 = Vector3f(base[index], base[index + 1], base[index + 2]);
		Vector3f p2 = Vector3f(top[index], top[index + 1], top[index + 2]);

		vectors[i] = (p2 - p1) / stacks;
	}

	for (unsigned int i = 1; i < stacks; i++)
	{
		for (unsigned int j = 0; j < 12; j++)
		{
			unsigned int index = 36 * i + j * 3;
			unsigned int baseIndex = j * 3;
			vertices[index] = base[baseIndex] + vectors[j].x * i;
			vertices[index + 1] = base[baseIndex + 1] + vectors[j].y * i;
			vertices[index + 2] = vertices[baseIndex + 2] + vectors[j].z * i;
 		}
	}
}

void MyRobot::calculateIndices(unsigned int stacks)
{
	indices = vector<GLushort>(24 * stacks * 3);
	for (unsigned int i = 0; i < stacks; i++)
	{
		unsigned int index = i * 24 * 3;
		unsigned int up = 12 * (i + 1);
		unsigned int down = 12 * i;

		for (unsigned int j = 0; j < 12; j++)
		{
			unsigned int k = index + j * 6;

			indices[k] = down + j;
			indices[k + 1] = up + j;
			indices[k + 2] = down + j + 1;

			indices[k + 3] = down + j + 1;
			indices[k + 4] = up + j;
			indices[k + 5] = up + j + 1;
		}

		indices[index + 11 * 6 + 2] = down;
		indices[index + 11 * 6 + 3] = down;
		indices[index + 11 * 6 + 5] = up;
	}
}

void MyRobot::calculateNormals(unsigned int stacks)
{
	const unsigned int nVertices = vertices.size();

	vector<Vector3f> normals = vector<Vector3f>(nVertices / 3, Vector3f(0.0f, 0.0f, 0.0f));
	for (unsigned int i = 0; i < indices.size(); i += 3)
	{
		unsigned int i1 = indices[i] * 3;
		unsigned int i2 = indices[i + 1] * 3;
		unsigned int i3 = indices[i + 2] * 3;

		vector<Vector3f> triangle = vector<Vector3f>(3);
		triangle[0] = Vector3f(vertices[i1], vertices[i1 + 1], vertices[i1 + 2]);
		triangle[1] = Vector3f(vertices[i2], vertices[i2 + 1], vertices[i2 + 2]);
		triangle[2] = Vector3f(vertices[i3], vertices[i3 + 1], vertices[i3 + 2]);

		Vector3f normal = calculateSurfaceNormal(triangle);
		normals[indices[i]] += normal;
		normals[indices[i + 1]] += normal;
		normals[indices[i + 2]] += normal;
	}

	this->normals = vector<GLfloat>(nVertices);
	for (unsigned int i = 0; i < normals.size(); i++)
	{
		unsigned int index = i * 3;
		normals[i].normalize();
		this->normals[index] = normals[i].x;
		this->normals[index + 1] = normals[i].y;
		this->normals[index + 2] = normals[i].z;
	}
}

void MyRobot::calculateTextels(unsigned int stacks)
{
	textels = vector<GLfloat>((stacks + 1) * 12 * 2);

	float angle = acos(-1) / 2.0f;
	float ry[2][2] = 
	{
		{ cos(angle),	sin(angle)	},
		{ -sin(angle),	cos(angle)	},
	};

	

	for (unsigned int i = 0; i < vertices.size() / 3; i++)
	{
		unsigned int vIndex = i * 3;
		Vector3f v = Vector3f(vertices[vIndex], vertices[vIndex + 1], vertices[vIndex + 2]);

		unsigned int tIndex = i * 2;
		float tx = 1.0f - v.x + 0.5f;
		float ty = v.z + 0.5f;

		textels[tIndex] = tx * ry[0][0] + ty * ry[1][0];
		textels[tIndex + 1] = tx * ry[0][1] + ty * ry[1][1];
 	}
}

void MyRobot::createMaterial()
{
	string filenames[3] = 
	{
		"robot1.jpg", "lavaCracks.jpg", "lamp.png",
	};

	materials = vector<CGFappearance*>(3);
	for (unsigned int i = 0; i < 3; i++)
	{
		materials[i] = new CGFappearance();
		materials[i]->setTexture(filenames[i]);
	}
}

Vector3f MyRobot::calculateSurfaceNormal(vector<Vector3f> triangle)
{
	Vector3f normal = Vector3f(0.0f, 0.0f, 0.0f);
	if(triangle.size() != 3)
		return normal;

	Vector3f v1 = triangle[1] - triangle[0];
	Vector3f v2 = triangle[2] - triangle[0];

	normal = v1.crossProduct(v2);

	return normal;
}