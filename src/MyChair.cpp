#include "MyChair.h"
#include <iostream>

MyChair::MyChair()
{
	//angle = rand() % 41 - 20;
	//xShift = (rand() % 9 - 4) / 10.0;
	//zShift = (rand() % 9 - 4) / 10.0;

	material = 0;
	createMaterial();
}

MyChair::~MyChair()
{
	if (material)
	{
		delete material;
		material = 0;
	}
}

void MyChair::draw()
{
	GLfloat baseWidth = 2.0f;
	GLfloat baseHeight = 0.2f;
	GLfloat baseDepth = 2.0f;

	GLfloat backWidth = 2.0f;
	GLfloat backHeight = 2.3f;
	GLfloat backDepth = 0.2f;

	GLfloat legWidth = 0.25f;
	GLfloat legHeight = 2.3f;
	GLfloat legDepth = 0.25f;

	material->apply();

	// Chair
	glPushMatrix();
	//glRotatef(angle, 0.0f, 1.0f, 0.0f);
	//glTranslatef(xShift, 0.0f, zShift);
	
	// Base + Back
	glPushMatrix();
	glTranslatef(0.0f, baseHeight / 2.0f + legHeight, 0.0f);

	// Base
	glPushMatrix();
	glScalef(baseWidth, baseHeight, baseDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End Base

	// Back
	glPushMatrix();
	glTranslatef(0.0f, (backHeight + baseHeight) / 2.0f, (backDepth - baseDepth) / 2.0);
	glScalef(backWidth, backHeight, backDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End Back

	glPopMatrix();
	// End Base + Back

	// 1st Leg
	glPushMatrix();
	glTranslatef((legWidth - baseWidth) / 2.0f, legHeight / 2.0f, (legDepth - baseDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 1st Leg

	// 2nd Leg
	glPushMatrix();
	glTranslatef((legWidth - baseWidth) / 2.0f, legHeight / 2.0f, (baseDepth - legDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 2nd Leg

	// 3rd Leg
	glPushMatrix();
	glTranslatef((baseWidth - legWidth) / 2.0f, legHeight / 2.0f, (baseDepth - legDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 3rd Leg

	// 4th Leg
	glPushMatrix();
	glTranslatef((baseWidth - legWidth) / 2.0f, legHeight / 2.0f, (legDepth - baseDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 4th Leg

	glPopMatrix();
	// End Chair
}

void MyChair::createMaterial()
{
	float amb [] = { 0.2f, 0.2f, 0.2f };
	float dif [] = { 1.0f, 0.3f, 0.0f };
	float spec [] = { 0.2f, 0.2f, 0.2f };
	float shininess = 20.0f;

	material = new CGFappearance(amb, dif, spec, shininess);
}