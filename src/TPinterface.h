#ifndef TPinterface_H
#define TPinterface_H

#include "CGFinterface.h"
#include "LightingScene.h"
#include "MyRobot.h"

#include <vector>

class TPinterface : public CGFinterface
{
public:
	TPinterface();
	~TPinterface();

	virtual void initGUI();
	virtual void processGUI(GLUI_Control *ctrl);

	virtual void processKeyboard(unsigned char key, int x, int y);

private:
	void processCheckboxes(GLUI_Control* ctrl);

private:
	LightingScene* lScene;
	MyRobot* robot;
	vector<CGFlight*> lights;
	vector<int*> lightCheckBoxes;
	bool clockUpdate;
	int textureValue;
	vector<string> textureNames;
	int polygonMode;
};


#endif
