#pragma once

#include "GL\gl.h"

class MyClockHand
{
public:
	MyClockHand();
	~MyClockHand();

	void draw();

	void addAngle(float angle);
	void setAngle(float angle);
	void setScalef(float x, float y, float z);

private:
	float angle;
	float scale[3];
};