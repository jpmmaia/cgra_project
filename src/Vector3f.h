#pragma once

#include <ostream>

struct Vector3f
{
	Vector3f();
	Vector3f(float x, float y, float z);

	float lengthXZ() const;
	float angleXZ() const;
	float length();
	void normalize();
	float dot(const Vector3f& v) const;
	Vector3f& crossProduct(const Vector3f& v);

	Vector3f& operator=(const Vector3f& v);
	bool operator==(const Vector3f& v) const;

	Vector3f operator+(const Vector3f& v) const;
	Vector3f operator-(const Vector3f& v) const;
	Vector3f operator*(const Vector3f& v) const;
	Vector3f operator/(const Vector3f& v) const;

	Vector3f& operator+=(const Vector3f& v);
	Vector3f& operator-=(const Vector3f& v);
	Vector3f& operator*=(const Vector3f& v);
	Vector3f& operator/=(const Vector3f& v);

	Vector3f operator*(float value) const;
	Vector3f operator/(float value) const;
	Vector3f& operator*=(float value);
	Vector3f& operator/=(float value);

	float x, y, z;
};

std::ostream& operator<<(std::ostream& os, const Vector3f& v);