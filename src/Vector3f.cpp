#include "Vector3f.h"

#include <ostream>

Vector3f::Vector3f() : x(0), y(0), z(0)
{
}
Vector3f::Vector3f(float x, float y, float z) : x(x), y(y), z(z)
{
}

float Vector3f::lengthXZ() const
{
	Vector3f v = Vector3f(x, 0.0f, z);
	return v.length();
}

float Vector3f::angleXZ() const
{
	Vector3f v = Vector3f(x, 0.0f, z);
	v.normalize();

	float angle = atan2(v.z, v.x);

	// Angle in range [0.0f, 2 * pi[
	if (angle < 0.0f)
		angle += 2.0f * (float) acos(-1);

	return angle;
}

float Vector3f::length()
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

void Vector3f::normalize()
{
	float length = this->length();
	if (length == 0)
		return;

	x /= length;
	y /= length;
	z /= length;
}

float Vector3f::dot(const Vector3f& v) const
{
	return x * v.x + y * v.y + z * v.z;
}

Vector3f& Vector3f::crossProduct(const Vector3f& v)
{
	float x = (this->y * v.z) - (v.y * this->z);
	float y = (v.x * this->z) - (this->x * v.z);
	float z = (this->x * v.y) - (v.x * this->y);

	this->x = x;
	this->y = y;
	this->z = z;

	return *this;
}

Vector3f& Vector3f::operator=(const Vector3f& v)
{
	x = v.x;
	y = v.y;
	z = v.z;

	return *this;
}
bool Vector3f::operator==(const Vector3f& v) const
{
	return x == v.x && y == v.y && z == v.z;
}

Vector3f Vector3f::operator+(const Vector3f& v) const
{
	return Vector3f(x + v.x, y + v.y, z + v.z);
}
Vector3f Vector3f::operator-(const Vector3f& v) const
{
	return Vector3f(x - v.x, y - v.y, z - v.z);
}
Vector3f Vector3f::operator*(const Vector3f& v) const
{
	return Vector3f(x * v.x, y * v.y, z * v.z);
}
Vector3f Vector3f::operator/(const Vector3f& v) const
{
	return Vector3f(x / v.x, y / v.y, z / v.z);
}

Vector3f& Vector3f::operator+= (const Vector3f& v)
{
	x += v.x;
	y += v.y;
	z += v.z;

	return *this;
}
Vector3f& Vector3f::operator-= (const Vector3f& v)
{
	y -= v.y;
	z -= v.z;

	return *this;
}
Vector3f& Vector3f::operator*=(const Vector3f& v)
{
	x *= v.x;
	y *= v.y;
	z *= v.z;

	return *this;
}
Vector3f& Vector3f::operator/=(const Vector3f& v)
{
	x /= v.x;
	y /= v.y;
	z /= v.z;

	return *this;
}

Vector3f Vector3f::operator*(float value) const
{
	return Vector3f(x * value, y * value, z * value);
}
Vector3f Vector3f::operator/(float value) const
{
	return Vector3f(x / value, y / value, z / value);
}
Vector3f& Vector3f::operator*=(float value)
{
	x *= value;
	y *= value;
	z *= value;

	return *this;
}
Vector3f& Vector3f::operator/=(float value)
{
	x /= value;
	y /= value;
	z /= value;

	return *this;
}

std::ostream& operator<<(std::ostream& os, const Vector3f& v)
{
	return os << "[" << v.x << ", " << v.y << ", " << v.z << "]";
}