#pragma once

#include "CGFobject.h"
#include "CGFappearance.h"

class Plane
{
public:
	Plane(void);
	Plane(int);
	~Plane(void);
	
	void draw();

	void setTextureRotation(float tRotation);
	void setTextureDimensions(float width, float height);
	void setTextureOffset(float width, float height);
	void setTextureRepeat(float sRepeat, float tRepeat);

private:
	int _numDivisions; // Number of triangles that constitute rows/columns
	float _si;
	float _ti;
	float _sf;
	float _tf;

	float _tRotation;
};

