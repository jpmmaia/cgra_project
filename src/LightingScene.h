#ifndef LightingScene_H
#define LightingScene_H

#include "CGFscene.h"
#include "CGFappearance.h"
#include "MyTable.h"
#include "Plane.h"
#include "MyChair.h"
#include "MyCylinder.h"
#include "MyLamp.h"
#include "MyClock.h"
#include "MyPaperPlane.h"
#include "MyRobot.h"
#include "MyWindowWall.h"

#include <vector>

class LightingScene : public CGFscene
{
public:
	void init();
	void display();
	virtual void update(unsigned long millis);

	~LightingScene();

	MyRobot* getRobot() const;
	std::vector<CGFlight*> getLights() const;
	
	void toggleClock(bool enable);


private:
	void createMaterials();

private:
	CGFlight* light0;
	CGFlight* light1;
	CGFlight* light2;
	CGFlight* light3;
	CGFlight* windowLight;

	MyLamp* lamp;
	MyTable* table;
	MyChair* chair;
	Plane* planeWall;
	Plane* floor;
	Plane* boardA;
	Plane* boardB;
	MyCylinder* collumn;
	MyClock* clock;
	MyPaperPlane* paperPlane;
	MyRobot* robot;
	MyWindowWall* leftWall;

	CGFappearance* slidesAppereance;
	CGFtexture* slidesTexture;

	CGFappearance* boardAppeareance;
	CGFtexture* boardTexture;

	CGFappearance* materialWall;
	CGFappearance* floorAppearance;

	bool clockUpdate;
};

#endif