#include "MyWindowWall.h"

using namespace std;

MyWindowWall::MyWindowWall()
{
	mPosition = Vector3f(0.0f, 5.0f, 7.5f);
	mRotation = Vector3f(0.0f, 90.0f, 0.0f);
	mScale = Vector3f(15.0f, 10.0f, 0.2f);

	const float textureOffset = 1.0f;
	const float holeWidth = 1.0f - 10.0f * textureOffset / 15.0f;
	const float holeHeight = 0.5f * textureOffset;

	calculateVertices(holeWidth, holeHeight);
	calculateIndices();
	calculateTextels(holeWidth, holeHeight);
	
	float amb[] = { 0.2f, 0.2f, 0.2f };
	float dif[] = { 0.9f, 0.9f, 0.9f };
	float spec[] = { 0.1f, 0.1f, 0.1f };
	float shininess = 20.0f;
	
	mMaterial = CGFappearance(amb, dif, spec, shininess);
	mMaterial.setTexture("window.png");
	mMaterial.setTextureWrap(GL_CLAMP, GL_CLAMP);
}

MyWindowWall::~MyWindowWall()
{
}

void MyWindowWall::draw()
{
	glPushMatrix();
	glTranslatef(mPosition.x, mPosition.y, mPosition.z);
	glRotatef(mRotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(mRotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(mRotation.z, 0.0f, 0.0f, 1.0f);
	glScalef(mScale.x, mScale.y, mScale.z);

	mMaterial.apply();

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, &mVertices[0]);
	glTexCoordPointer(2, GL_FLOAT, 0, &mTextels[0]);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_BYTE, &mIndices[0]);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();

	mLandscape.draw();
}

void MyWindowWall::calculateVertices(const float holeWidth, const float holeHeight)
{
	GLfloat xShift = (1.0f - holeWidth) / 2.0f;
	GLfloat yShift = (1.0f - holeHeight) / 2.0f;
	GLfloat x[] = { -0.5f, -0.5f + xShift, 0.5f - xShift, 0.5f };
	GLfloat y[] = { -0.5f, -0.5f + yShift, 0.5f - yShift, 0.5f };
	mVertices = vector<GLfloat>(48);
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			unsigned int index = 3 * (j + i * 4);
			mVertices[index] = x[j];
			mVertices[index + 1] = y[i];
			mVertices[index + 2] = 0.0f;
		}
	}
}

void MyWindowWall::calculateIndices()
{
	mIndices = vector<GLubyte>(48);
	for (unsigned int i = 0, offset = 0; i < 11; i++)
	{
		if (i == 3 || i == 5 || i == 7)
		{
			i++;
			offset += 6;
		}

		unsigned int index = i * 6 - offset;
		mIndices[index] = i;
		mIndices[index + 1] = i + 1;
		mIndices[index + 2] = i + 5;
		

		mIndices[index + 3] = i;
		mIndices[index + 4] = i + 5;
		mIndices[index + 5] = i + 4;
	}
}

void MyWindowWall::calculateTextels(const float holeWidth, const float holeHeight)
{
	float matrix[3][3] =
	{
		{ 0.94f / holeWidth, 0.0f, 0.5f },
		{ 0.0f, 0.94f / holeHeight, 0.5f },
		{ 0.0f, 0.0f, 1.0f },
	};

	mTextels = vector<GLfloat>(32);
	for (unsigned int i = 0; i < mTextels.size(); i += 2)
	{
		unsigned int vIndex = i * 3 / 2;

		mTextels[i] = mVertices[vIndex] * matrix[0][0] + mVertices[vIndex + 1] * matrix[0][1] + 1.0f * matrix[0][2];
		mTextels[i + 1] = mVertices[vIndex] * matrix[1][0] + mVertices[vIndex + 1] * matrix[1][1] + 1.0f * matrix[1][2];
	}
}