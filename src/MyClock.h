#pragma once

#include "MyCylinder.h"
#include "CGFappearance.h"
#include "MyClockHand.h"

class MyClock
{
public:
	MyClock();
	~MyClock();

	void draw();
	void update(unsigned long millis);
	void reinitialize();

private:
	static const unsigned int HOURS = 0;
	static const unsigned int MINUTES = 1;
	static const unsigned int SECONDS = 2;

	bool firstUpdate;
	unsigned int lastMillis;

	MyCylinder* clock;
	MyClockHand* clockHands[3];
	CGFappearance* appearance;
};