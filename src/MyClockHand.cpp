#include "MyClockHand.h"

MyClockHand::MyClockHand()
{
	angle = 0.0f;
	
	for (unsigned int i = 0; i < 3; i++)
		scale[i] = 1.0f;
}
MyClockHand::~MyClockHand()
{

}

void MyClockHand::draw()
{
	glPushMatrix();

	glRotatef(angle, 0.0f, 0.0f, -1.0f);
	glScalef(scale[0], scale[1], scale[2]);
	glNormal3f(0.0f, 0.0f, 1.0f);
	
	glBegin(GL_TRIANGLE_STRIP);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.18f, 0.0f);
	glEnd();
	
	glPopMatrix();
}

void MyClockHand::addAngle(float angle)
{
	this->angle += angle;
}

void MyClockHand::setAngle(float angle)
{
	this->angle = angle;
}

void MyClockHand::setScalef(float x, float y, float z)
{
	scale[0] = x;
	scale[1] = y;
	scale[2] = z;
}