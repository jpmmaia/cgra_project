#include "Plane.h"

#include <GL/glu.h>


Plane::Plane(void)
{
	_numDivisions = 1;

	_si = 0;
	_ti = 0;
	_sf = 1;
	_tf = 1;

	_tRotation = 0;
}

Plane::Plane(int n)
{
	_numDivisions = n;

	_si = 0;
	_ti = 0;
	_sf = 1;
	_tf = 1;

	_tRotation = 0;
}


Plane::~Plane(void)
{
}

void Plane::draw()
{
	glPushMatrix();
	glRotatef(180.0, 1, 0, 0);
	glTranslatef(-0.5, 0.0, -0.5);
	glScalef(1.0 / (double) _numDivisions, 1, 1.0 / (double) _numDivisions);
	glNormal3f(0, -1, 0);

	// Rotating Texture
	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glTranslatef(0.5f, 0.5f, 0.0f);
	glRotatef(_tRotation, 0.0f, 0.0f, 1.0f);
	glTranslatef(-0.5f, -0.5f, 0.0f);
	glMatrixMode(GL_MODELVIEW);

	for (int bx = 0; bx < _numDivisions; bx++)
	{
		float xShift = (float) bx * (_sf - _si) / _numDivisions;
		float xNextShift = (float) (bx + 1) * (_sf - _si) / _numDivisions;

		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(_si + xShift, _ti);
		glVertex3f(bx, 0, 0);

		for (int bz = 0; bz < _numDivisions; bz++)
		{
			float yShift = (float) bz * (_tf - _ti) / _numDivisions;
			float yNextShift = (float) (bz + 1) * (_tf - _ti) / _numDivisions;

			glTexCoord2f(_si + xNextShift, _ti + yShift);
			glVertex3f(bx + 1, 0, bz);

			glTexCoord2f(_si + xShift, _ti + yNextShift);
			glVertex3f(bx, 0, bz + 1);
		}

		glTexCoord2f(_si + xNextShift, _tf);
		glVertex3d(bx + 1, 0, _numDivisions);

		glEnd();
	}

	// Unrotating texture
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glPopMatrix();
}

void Plane::setTextureDimensions(float width, float height)
{
	float ratio = width / height;

	if (ratio > 1)
	{
		float shift = (ratio - 1.0f) / 2.0f;
		
		_ti = -shift;
		_tf = 1.0f + shift;
	}
	else
	{
		float shift = (1.0f / ratio - 1.0f) / 2.0f;

		_si = -shift;
		_sf = 1.0f + shift;
	}
}

void Plane::setTextureOffset(float width, float height)
{
	float xShift = width / 2.0f;
	float yShift = height / 2.0f;

	_si = -xShift;
	_sf = 1.0f + xShift;

	_ti = -yShift;
	_tf = 1.0f + yShift;
}

void Plane::setTextureRepeat(float sRepeat, float tRepeat)
{
	_si = 0.0f;
	_ti = 0.0f;

	_sf = sRepeat;
	_tf = tRepeat;
}

void Plane::setTextureRotation(float tRotation)
{
	_tRotation = tRotation;
}