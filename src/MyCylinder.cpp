#include "MyCylinder.h"
#include <math.h>

MyCylinder::MyCylinder(int slices, int stacks, bool smooth)
{
	float pi = acos(-1.0f);
	theta = 2 * pi / slices;

	yShift = 1.0f / stacks;

	this->slices = slices;
	this->stacks = stacks;
	this->smooth = smooth;

	createMaterials();
}

MyCylinder::~MyCylinder()
{
	if (sideAppearance)
	{
		delete sideAppearance;
		sideAppearance = 0;
	}
	if (baseAppearance)
	{
		delete baseAppearance;
		baseAppearance = 0;
	}
}

void MyCylinder::draw()
{
	float angle = 0.0f;

	float normalAngle = theta / 2.0f;
	if (smooth)
		normalAngle = 0.0f;
	else
		normalAngle = theta / 2.0f;

	sideAppearance->apply();

	float yA = 0.0f;
	float yB = yA + yShift;
	for (int j = 0; j < stacks; j++)
	{
		for (int i = 0; i < slices; i++)
		{
			if (smooth)
				drawSideFaceSmooth(yA, yB, angle, normalAngle, i, j);
			else
				drawSideFaceFlat(yA, yB, angle, normalAngle, i, j);
		}

		yA = yB;
		yB += yShift;
	}

	baseAppearance->apply();

	// Draw lower base
	glPushMatrix();
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	drawBase();
	glPopMatrix();

	// Draw upper base
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	drawBase();
	glPopMatrix();

}

void MyCylinder::drawBase()
{
	float angle = 0.0f;
	float xA, xB, zA, zB;

	for (int i = 0; i < slices; i++)
	{
		xA = cos(angle);
		zA = sin(angle);

		angle += theta;

		xB = cos(angle);
		zB = sin(angle);

		glNormal3f(0.0f, 1.0f, 0.0f);
		glBegin(GL_TRIANGLES);
		glTexCoord2f((1.0f + xB) / 2.0f, (1.0f - zB) / 2.0f);
		glVertex3f(xB, 0.0f, zB);

		glTexCoord2f((1.0f + xA) / 2.0f, (1.0f - zA) / 2.0f);
		glVertex3f(xA, 0.0f, zA);

		glTexCoord2f(0.5f, 0.5f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glEnd();
	}
}

void MyCylinder::drawSideFaceFlat(float yA, float yB, float &angle, float &normalAngle, int sliceIter, int stackIter)
{
	float xA = cos(angle);
	float zA = sin(angle);

	angle += theta;

	float xB = cos(angle);
	float zB = sin(angle);

	glNormal3f(cos(normalAngle), 0.0f, sin(normalAngle));
	normalAngle += theta;

	float sText = (float) sliceIter / slices;
	float sNextText = (float) (sliceIter + 1) / slices;
	float tText = (float) stackIter / stacks;
	float tNextText = (float) (stackIter + 1) / stacks;

	glBegin(GL_QUADS);
	glTexCoord2f(sText, tText);
	glVertex3f(xA, yA, zA);
	
	glTexCoord2f(sText, tNextText);
	glVertex3f(xA, yB, zA);
	
	glTexCoord2f(sNextText, tNextText);
	glVertex3f(xB, yB, zB);
	
	glTexCoord2f(sNextText, tText);
	glVertex3f(xB, yA, zB);
	glEnd();
}

void MyCylinder::drawSideFaceSmooth(float yA, float yB, float &angle, float &normalAngle, int sliceIter, int stackIter)
{
	float xA = cos(angle);
	float zA = sin(angle);

	angle += theta;

	float xB = cos(angle);
	float zB = sin(angle);

	float sText = (float) sliceIter / slices;
	float sNextText = (float) (sliceIter + 1) / slices;
	float tText = (float) stackIter / stacks;
	float tNextText = (float) (stackIter + 1) / stacks;

	glBegin(GL_QUADS);
	glNormal3f(cos(normalAngle), 0.0f, sin(normalAngle));

	glTexCoord2f(sText, tText);
	glVertex3f(xA, yA, zA);

	glTexCoord2f(sText, tNextText);
	glVertex3f(xA, yB, zA);

	normalAngle += theta;
	glNormal3f(cos(normalAngle), 0.0f, sin(normalAngle));

	glTexCoord2f(sNextText, tNextText);
	glVertex3f(xB, yB, zB);

	glTexCoord2f(sNextText, tText);
	glVertex3f(xB, yA, zB);
	glEnd();
}

void MyCylinder::createMaterials()
{
	float ambCollumn[] = { 0.2f, 0.2f, 0.2f };
	float difCollumn[] = { 0.8f, 0.8f, 0.8f };
	float specCollumn[] = { 0.3f, 0.3f, 0.13 };
	float shininessCollumn = 40.0f;
	sideAppearance = new CGFappearance(ambCollumn, difCollumn, specCollumn, shininessCollumn);
	baseAppearance = new CGFappearance(ambCollumn, difCollumn, specCollumn, shininessCollumn);
}

void MyCylinder::setSideAppearance(CGFappearance* sideAppearance)
{
	if (this->sideAppearance)
		delete this->sideAppearance;

	this->sideAppearance = sideAppearance;
}
void MyCylinder::setBaseAppearance(CGFappearance* baseAppearance)
{
	if (this->baseAppearance)
		delete this->baseAppearance;

	this->baseAppearance = baseAppearance;
}