#pragma once

#include "CGFobject.h"
#include "Vector3f.h"
#include "CGFappearance.h"

#include <math.h>
#include <vector>

class MyRobot : CGFobject
{
public:
	MyRobot();
	~MyRobot();

	bool initialize(unsigned int stacks);
	void draw();

	void move(float value);

	void addPosition(Vector3f position);
	void setPosition(Vector3f position);

	void addRotation(Vector3f rotation);
	void setRotation(Vector3f rotation);

	void setTexture(unsigned int index);
	void setWireframeMode(bool enable);

private:
	void calculateVertices(unsigned int stacks);
	void calculateIndices(unsigned int stacks);
	void calculateNormals(unsigned int stacks);
	void calculateTextels(unsigned int stacks);
	void createMaterial();

	static Vector3f calculateSurfaceNormal(vector<Vector3f> triangle);

private:
	static const float degree2rad;
	static const float rad2degree;

	Vector3f position;
	Vector3f rotation;

	unsigned int stacks;
	std::vector<GLfloat> vertices;
	std::vector<GLushort> indices; 
	std::vector<GLfloat> normals;
	std::vector<GLfloat> textels;

	std::vector<CGFappearance*> materials;
	unsigned int currentTexture;

	bool wireframeMode;
};