#include "TPinterface.h"
#include "LightingScene.h"

#include <string>
#include <iostream>

using namespace std;

TPinterface::TPinterface()
{
	lScene = 0;
	robot = 0;
	lightCheckBoxes = vector<int*>(0);
}

TPinterface::~TPinterface()
{
	for (unsigned int i = 0; i < lightCheckBoxes.size(); i++)
	{
		if (lightCheckBoxes[i])
		{
			delete lightCheckBoxes[i];
			lightCheckBoxes[i] = 0;
		}
	}
}


void TPinterface::processKeyboard(unsigned char key, int x, int y)
{
	// Uncomment below if you would like to process the default keys (e.g. 's' for snapshot, 'Esc' for exiting, ...)
	// CGFinterface::processKeyboard(key, x, y);

	switch(key)
	{
	case 'j': case 'J':
		robot->addRotation(Vector3f(0.0f, 3.0f, 0.0f));
		break;
	case 'l': case 'L':
		robot->addRotation(Vector3f(0.0f, -3.0f, 0.0f));
		break;

	case 'i': case 'I':
		robot->move(0.1f);
		break;
	case 'k': case 'K':
		robot->move(-0.1f);
		break;
	}
}

void TPinterface::initGUI()
{
	lScene = (LightingScene*) scene;
	robot = lScene->getRobot();
	lights = lScene->getLights();
	clockUpdate = true;
	textureValue = 0;
	polygonMode = 0;

	GLUI_Panel* optionsPanel = addPanel("Opcoes");

	GLUI_Panel* lightsPanel = addPanelToPanel(optionsPanel, "Luzes", 1);
	addColumnToPanel(optionsPanel);
	lightCheckBoxes = vector<int*>(5);
	for (unsigned int i = 0; i < lightCheckBoxes.size(); i++)
		lightCheckBoxes[i] = new int(1);

	this->addCheckboxToPanel(lightsPanel, "Luz 0", lightCheckBoxes[0], 0);
	this->addCheckboxToPanel(lightsPanel, "Luz 1", lightCheckBoxes[1], 1);
	this->addCheckboxToPanel(lightsPanel, "Luz 2", lightCheckBoxes[2], 2);
	this->addCheckboxToPanel(lightsPanel, "Luz 3", lightCheckBoxes[3], 3);
	this->addCheckboxToPanel(lightsPanel, "Luz 4", lightCheckBoxes[4], 4);

	GLUI_Panel* viewModePanel = this->addPanelToPanel(optionsPanel, "Modo de visualizacao");
	addColumnToPanel(optionsPanel);
	GLUI_Listbox* listBox = this->addListboxToPanel(viewModePanel, "Textura do Robot ", &textureValue, 6);
	listBox->add_item(0, "robot1.jpg");
	listBox->add_item(1, "lavaCracks.jpg");
	listBox->add_item(2, "lamp.png");

	GLUI_RadioGroup* polygonModeGroup = this->addRadioGroupToPanel(viewModePanel, &polygonMode, 7);
	this->addRadioButtonToGroup(polygonModeGroup, "Textured");
	this->addRadioButtonToGroup(polygonModeGroup, "Wireframe");

	GLUI_Panel* animationPanel = addPanelToPanel(optionsPanel, "Animacoes");
	this->addButtonToPanel(animationPanel, "Relogio", 5);

}

void TPinterface::processGUI(GLUI_Control* ctrl)
{
	long id = ctrl->user_id;

	switch (id)
	{
	case 0: case 1: case 2: case 3: case 4:
		processCheckboxes(ctrl);
		break;
	case 5:
		clockUpdate = !clockUpdate;
		lScene->toggleClock(clockUpdate);
		break;
	case 6:
		robot->setTexture(textureValue);
		break;
	case 7:
		robot->setWireframeMode(polygonMode == 1);
		break;
	}

	printf ("GUI control id: %d\n  ",ctrl->user_id);
}

void TPinterface::processCheckboxes(GLUI_Control* ctrl)
{
	long id = ctrl->user_id;

	if (*lightCheckBoxes[id] != 0)
		lights[id]->enable();
	else
		lights[id]->disable();
}

