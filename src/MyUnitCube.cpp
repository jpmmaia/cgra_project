#include "MyUnitCube.h"

void MyUnitCube::draw()
{
	// Front
	glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.5f);
		glNormal3f(0.0f, 0.0f, 1.0f);
		drawRect();
	glPopMatrix();

	// Right
	glPushMatrix();
		glTranslatef(0.5f, 0.0f, 0.0f);
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glNormal3f(0.0f, 0.0f, 1.0f);
		drawRect();
	glPopMatrix();

	// Back
	glPushMatrix();
		glTranslatef(0.0f, 0.0f, -0.5f);
		glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
		glNormal3f(0.0f, 0.0f, 1.0f);
		drawRect();
	glPopMatrix();

	// Left
	glPushMatrix();
		glTranslatef(-0.5f, 0.0f, 0.0f);
		glRotatef(270.0f, 0.0f, 1.0f, 0.0f);
		glNormal3f(0.0f, 0.0f, 1.0f);
		drawRect();
	glPopMatrix();

	// Lower
	glPushMatrix();
		glTranslatef(0.0f, -0.5f, 0.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		glNormal3f(0.0f, 0.0f, 1.0f);
		drawRect();
	glPopMatrix();

	// Top
	glPushMatrix();
		glTranslatef(0.0f, 0.5f, 0.0f);
		glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
		glNormal3f(0.0f, 0.0f, 1.0f);
		drawRect();
	glPopMatrix();
}

void MyUnitCube::drawRect()
{
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	
	glTexCoord2f(1, 0);
	glVertex3f(0.5f, -0.5f, 0.0f);
	
	glTexCoord2f(1, 1);
	glVertex3f(0.5f, 0.5f, 0.0f);
	
	glTexCoord2f(0, 1);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glEnd();
}