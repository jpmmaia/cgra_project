#include "MyTable.h"

MyTable::MyTable()
{
	//angle = rand() % 21 - 10;
	//xShift = (rand() % 9 - 4) / 10.0;
	//zShift = (rand() % 9 - 4) / 10.0;

	createTableAppearance();
	createLegsMaterial();
}

MyTable::~MyTable()
{
	if (tableAppearance)
	{
		delete tableAppearance;
		tableAppearance = 0;
	}

	if (legsMaterial)
	{
		delete legsMaterial;
		legsMaterial = 0;
	}
}

void MyTable::draw()
{
	// Legs
	GLfloat legWidth = 0.3f;
	GLfloat legHeight = 3.5f;
	GLfloat legDepth = 0.3f;

	GLfloat tabletopWidth = 5.0f;
	GLfloat tabletopHeight = 0.3f;
	GLfloat tabletopDepth = 3.0f;

	// Table
	glPushMatrix();
	//glRotatef(angle, 0.0f, 1.0f, 0.0f);
	//glTranslatef(xShift, 0.0f, zShift);

	legsMaterial->apply();

	// 1st Leg
	glPushMatrix();
	glTranslatef((legWidth - tabletopWidth) / 2.0f, legHeight / 2.0f, (legDepth - tabletopDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 1st Leg

	// 2nd Leg
	glPushMatrix();
	glTranslatef((legWidth - tabletopWidth) / 2.0f, legHeight / 2.0f, (tabletopDepth - legDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 2nd Leg

	// 3rd Leg
	glPushMatrix();
	glTranslatef((tabletopWidth - legWidth) / 2.0f, legHeight / 2.0f, (tabletopDepth - legDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 3rd Leg

	// 4th Leg
	glPushMatrix();
	glTranslatef((tabletopWidth - legWidth) / 2.0f, legHeight / 2.0f, (legDepth - tabletopDepth) / 2.0f);
	glScalef(legWidth, legHeight, legDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End 4th Leg

	// Tabletop
	tableAppearance->apply();
	
	glPushMatrix();
	glTranslatef(0.0f, legHeight + tabletopHeight / 2.0f, 0.0f);
	glScalef(tabletopWidth, tabletopHeight, tabletopDepth);
	myUnitCube.draw();
	glPopMatrix();
	// End Tabletop

	glPopMatrix();
	// End Table
}

void MyTable::createTableAppearance()
{
	float ambient[3] = { 0.2f, 0.2f, 0.2f };
	float diffuse[3] = { 0.4f, 0.2f, 0.0f };
	float specularity[3] = { 0.2f, 0.2f, 0.2f };
	float shininess = 20.0f;

	tableAppearance = new CGFappearance(ambient, diffuse, specularity, shininess);
	tableAppearance->setTexture("table.png");
}

void MyTable::createLegsMaterial()
{
	float ambient[3] = { 0.2f, 0.2f, 0.2f };
	float diffuse[3] = { 0.572f, 0.579f, 0.598f };
	float specularity[3] = { 1.0f, 1.0f, 0.984f };
	float shininess = 80.0f;

	legsMaterial = new CGFappearance(ambient, diffuse, specularity, shininess);
}