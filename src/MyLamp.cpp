#include "MyLamp.h"

using namespace std;

static const float PI = acos(-1);
static const float PI_2 = acos(-1) / 2.0f;

MyLamp::MyLamp(int stacks, int slices)
{
	theta = PI / slices;
	phi = PI / stacks;

	this->slices = slices;
	this->stacks = stacks;

	createMaterial();
}

void MyLamp::draw()
{
	material->apply();

	for (int i = 0; i < stacks; i++)
	{
		for (int j = 0; j < slices; j++)
		{
			float thetaA = PI - theta * j;
			float thetaB = PI - theta * (j + 1);

			float phiA = phi * i;
			float phiB = phi * (i + 1);

			float sText = (float) j / slices;
			float sNextText = (float) (j + 1) / slices;
			float tText = (float) i / stacks;
			float tNextText = (float) (i + 1) / stacks;

			glBegin(GL_QUADS);
			glTexCoord2f(sText, tText);
			glNormal3f(sin(thetaA) * cos(phiA), sin(thetaA) * sin(phiA), cos(thetaA));
			glVertex3f(sin(thetaA) * cos(phiA), sin(thetaA) * sin(phiA), cos(thetaA));

			glTexCoord2f(sText, tNextText);
			glNormal3f(sin(thetaA) * cos(phiB), sin(thetaA) * sin(phiB), cos(thetaA));
			glVertex3f(sin(thetaA) * cos(phiB), sin(thetaA) * sin(phiB), cos(thetaA));

			glTexCoord2f(sNextText, tNextText);
			glNormal3f(sin(thetaB) * cos(phiB), sin(thetaB) * sin(phiB), cos(thetaB));
			glVertex3f(sin(thetaB) * cos(phiB), sin(thetaB) * sin(phiB), cos(thetaB));

			glTexCoord2f(sNextText, tText);
			glNormal3f(sin(thetaB) * cos(phiA), sin(thetaB) * sin(phiA), cos(thetaB));
			glVertex3f(sin(thetaB) * cos(phiA), sin(thetaB) * sin(phiA), cos(thetaB));
			glEnd();
		}
	}
}

void MyLamp::createMaterial()
{
	float amb[3] = { 0.2f, 0.2f, 0.2f};
	float diff[3] = { 1.0f, 1.0f, 1.0f };
	float spec[3] = { 0.5f, 0.5f, 0.5f };
	float shininess = 50.0f;

	material = new CGFappearance(amb, diff, spec, shininess);
	material->setTexture("lamp.png");
}

MyLamp::~MyLamp()
{
	if (material)
	{
		delete material;
		material = 0;
	}
}