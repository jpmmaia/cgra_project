#ifndef MY_WINDOW_WALL_H
#define MY_WINDOW_WALL_H

#include "CGFobject.h"
#include "CGFappearance.h"
#include "MyLandscape.h"
#include "Vector3f.h"

#include <vector>

class MyWindowWall : CGFobject
{
public:
	MyWindowWall();
	~MyWindowWall();

	void draw();

private:
	void calculateVertices(const float holeWidth, const float holeHeight);
	void calculateIndices();
	void calculateTextels(const float holeWidth, const float holeHeight);

private:
	Vector3f mPosition;
	Vector3f mRotation;
	Vector3f mScale;

	std::vector<GLfloat> mVertices;
	std::vector<GLubyte> mIndices;
	std::vector<GLfloat> mTextels;

	CGFappearance mMaterial;
	
	MyLandscape mLandscape;
};

#endif