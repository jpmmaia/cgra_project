#ifndef MYFLOOR_H
#define MYFLOOR_H

#include "MyUnitCube.h"

class MyFloor : CGFobject
{
private:
	MyUnitCube floor;

public:
	void draw();
};

#endif // !MYFLOOR_H