#include "MyPaperPlane.h"

#include <math.h>
#include <iostream>

MyPaperPlane::MyPaperPlane()
{
	float width = 0.25f;
	float height = 0.125f;
	float depth = 1.0f;

	setDimensions(width, height, depth);

	// Declaring Normals
	GLfloat normalsArray [][3] =
	{
		{ -1.0f, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }, { 6.0f, 1.0f, 0.0f },
		{ -6.0f, 1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 1.0f, 0.0f, 0.0f },
	};

	unsigned int normalsArraySize = sizeof(normalsArray) / (sizeof(GLfloat) * 3);
	normals = std::vector<std::vector<GLfloat>>(normalsArraySize, std::vector<GLfloat>(3));
	for (unsigned int i = 0; i < normals.size(); i++)
	{
		for (unsigned int j = 0; j < 3; j++)
			normals[i][j] = normalsArray[i][j];
	}

	this->position = Vector3f(10.5f, 3.75f + height / 2.0f, 8.0f - depth / 2.0f);
	this->position0 = position;
	this->rotation = Vector3f(0.0f, -90.0f, 0.0f);
	this->rotation0 = rotation;
	this->acceleration = Vector3f(-0.02f, 0.01f, 0.0f);

	firstUpdate = true;
}
MyPaperPlane::~MyPaperPlane()
{
}

void MyPaperPlane::draw()
{
	std::vector<std::vector<GLfloat>>::const_iterator vertex;
	std::vector<std::vector<GLfloat>>::const_iterator normal;

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glRotatef(rotation0.x + rotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(rotation0.y + rotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(rotation0.z + rotation.z, 0.0f, 0.0f, 1.0f);

	glBegin(GL_QUAD_STRIP);
	// Drawing faces
	for (vertex = vertices.cbegin(), normal = normals.cbegin(); vertex != vertices.cend() && normal != normals.cend(); vertex++, normal++)
	{
		glNormal3f(normal->at(0), normal->at(1), normal->at(2));

		glVertex3f(vertex->at(0), vertex->at(1), vertex->at(2));
		vertex++;
		glVertex3f(vertex->at(0), vertex->at(1), vertex->at(2));
	}
	glEnd();

	glPopMatrix();
}
void MyPaperPlane::update(float millis)
{
	if (firstUpdate)
	{
		t0 = millis;
		firstUpdate = false;
		return;
	}

	float t = (millis - t0) / 1000.0f;
	position = rt(t, Vector3f(0.0f, 0.0f, 0.0f), position0);
	//std::cout << position << std::endl;
	rotation = vt(t, Vector3f(0.0f, 0.0f, 0.0f));
}

void MyPaperPlane::setPosition(float x, float y, float z)
{
	position0.x = x;
	position0.y = y;
	position0.z = z;
}
void MyPaperPlane::setRotation(float xAngle, float yAngle, float zAngle)
{
	rotation0.x = xAngle;
	rotation0.y = yAngle;
	rotation0.z = zAngle;
}
void MyPaperPlane::setDimensions(float width, float height, float depth)
{
	dimensions.x = width;
	dimensions.y = height;
	dimensions.z = depth;

	// Declaring vertices
	GLfloat verticesArray [][3] =
	{
		{ -width / 2.0f, height / 2.0f, 0.0f }, { -width / 2.0f, height / 2.0f, 0.1875f * depth },
		{ -width / 2.0f, height, 0.0f }, { -width / 2.0f, height, 0.375f * depth },
		{ -width / 40.0f, height, 0.0f }, { -width / 40.0f, height, 0.75f * depth },

		{ 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, depth },

		{ width / 40.0f, height, 0.0f }, { width / 40.0f, height, 0.75f * depth },
		{ width / 2.0f, height, 0.0f }, { width / 2.0f, height, 0.375f * depth },
		{ width / 2.0f, height / 2.0f, 0.0f }, { width / 2.0f, height / 2.0f, 0.1875f * depth },
	};

	unsigned int verticesArraySize = sizeof(verticesArray) / (sizeof(GLfloat) * 3);
	vertices = std::vector<std::vector<GLfloat>>(verticesArraySize, std::vector<GLfloat>(3));
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		for (unsigned int j = 0; j < 3; j++)
			vertices[i][j] = verticesArray[i][j];
	}
}
void MyPaperPlane::setAcceleration(float x, float y, float z)
{
	acceleration.x = x;
	acceleration.y = y;
	acceleration.z = z;
}

Vector3f MyPaperPlane::rt(float t, const Vector3f& r, const Vector3f& r0) const
{
	return vt(t, r) * pow(t, 2) + r * t + r0;
}
Vector3f MyPaperPlane::vt(float t, const Vector3f& r) const
{
	return acceleration * t + r;
}