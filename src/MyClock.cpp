#include "MyClock.h"

#include <math.h>
#include <iostream>

MyClock::MyClock()
{
	clock = new MyCylinder(12, 1, false);
	appearance = new CGFappearance();
	appearance->setTexture("clock.png");

	clock->setBaseAppearance(appearance);

	// Setting time to 3h 30m 45s
	float time[3] = { 90.0f, 180.0f, 270.0f };

	// Setting scale
	float scale[3][3] =
	{
		{ 0.1f, 0.5f, 1.0f }, { 0.1f, 0.7f, 1.0f }, { 0.025f, 0.9f, 1.0f },
	};

	for (unsigned int i = 0; i < 3; i++)
	{
		clockHands[i] = new MyClockHand();
		clockHands[i]->setAngle(time[i]);
		clockHands[i]->setScalef(scale[i][0], scale[i][1], scale[i][2]);
	}

	firstUpdate = true;
}

MyClock::~MyClock()
{
	if (clock)
	{
		delete clock;
		clock = 0;
	}
	for (int i = 0; i < 3; i++)
	{
		if (clockHands[i])
		{
			delete clockHands[i];
			clockHands[i] = 0;
		}
	}

}

void MyClock::draw()
{
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glScalef(1.0f, 0.1f, 1.0f);
	clock->draw();
	glPopMatrix();


	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.11f);
	
	// Clock hands will have a zShift between them
	float zShift = 0.01f;
	for (unsigned int i = 0; i < 3; i++)
	{
		glTranslatef(0.0f, 0.0f, zShift);
		clockHands[i]->draw();
	}

	glPopMatrix();
}

void MyClock::update(unsigned long millis)
{
	if (firstUpdate)
	{
		lastMillis = millis;
		firstUpdate = false;
		return;
	}

	float deltaMillis = millis - lastMillis;
	lastMillis = millis;

	float angles[3] = 
	{
		0.0001f * deltaMillis / 12.0f,
		0.0001f * deltaMillis,
		0.006f * deltaMillis
	};

	for (unsigned int i = 0; i < 3; i++)
		clockHands[i]->addAngle(angles[i]);
}

void MyClock::reinitialize()
{
	firstUpdate = true;
}