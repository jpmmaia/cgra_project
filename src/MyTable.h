#ifndef MYTABLE_h
#define MYTABLE_H

#include "MyUnitCube.h"
#include "CGFAppearance.h"

class MyTable : CGFobject
{
private:
	MyUnitCube myUnitCube;
	float angle;
	float xShift;
	float zShift;

	CGFappearance* tableAppearance;
	CGFappearance* legsMaterial;

public:
	MyTable();
	~MyTable();
	
	void draw();

private:
	void createTableAppearance();
	void createLegsMaterial();
};

#endif // !MYTABLE_h