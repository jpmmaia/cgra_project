#ifndef MY_CYLINDER
#define MY_CYLINDER

#include "CGFobject.h"
#include "CGFappearance.h"

class MyCylinder : public CGFobject
{
public:
	MyCylinder(int slices, int stacks, bool smooth);
	~MyCylinder();
	void draw();

	void setSideAppearance(CGFappearance* sideAppearance);
	void setBaseAppearance(CGFappearance* baseAppearance);

private:
	void drawBase();
	
	void drawSideFaceFlat(float yA, float yB, float &angle, float &normalAngle, int sliceIter, int stackIter);
	void drawSideFaceSmooth(float yA, float yB, float &angle, float &normalAngle, int sliceIter, int stackIter);

	void createMaterials();

private:
	int slices;
	int stacks;
	bool smooth;
	CGFappearance* sideAppearance;
	CGFappearance* baseAppearance;

	double yShift;
	float theta;
};



#endif
