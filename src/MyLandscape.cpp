#include "MyLandscape.h"

using namespace std;

MyLandscape::MyLandscape()
{
	unsigned int segments = 32;
	unsigned int rings = 16;

	calculateVertices(segments, rings);
	calculateIndices(segments, rings);
	calculateNormals();
	calculateTextels();

	mMaterial = CGFappearance();
	mMaterial.setTexture("landscape.jpg");

	mPosition = Vector3f(0.0f, 5.0f, 7.5f);
	mRotation = Vector3f(0.0f, -90.0f, 0.0f);
	mScale = Vector3f(5.0f, 5.0f, 5.0f);
}

MyLandscape::~MyLandscape()
{
}

void MyLandscape::draw()
{
	glPushMatrix();
	glTranslatef(mPosition.x, mPosition.y, mPosition.z);
	glRotatef(mRotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(mRotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(mRotation.z, 0.0f, 0.0f, 1.0f);
	glScalef(mScale.x, mScale.y, mScale.z);
	
	mMaterial.apply();

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, &mVertices[0]);
	glNormalPointer(GL_FLOAT, 0, &mNormals[0]);
	glTexCoordPointer(2, GL_FLOAT, 0, &mTextels[0]);
	glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_SHORT, &mIndices[0]);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();
}

void MyLandscape::calculateVertices(unsigned int segments, unsigned int rings)
{
	// Theta -> [0, 90] <-
	// Phi -> [0, 360[ ->

	float deltaPhi = 2 * acos(-1) / segments;
	float deltaTheta = acos(-1) / (2 * rings);

	mVertices = std::vector<GLfloat>((segments * rings + 1) * 3);
	mVertices[0] = 0.0f;
	mVertices[1] = 0.0f;
	mVertices[2] = 1.0f;

	unsigned int index = 3;
	for (unsigned int s = 0; s < segments; s++)
	{
		for (unsigned int r = 1; r <= rings; r++)
		{
			float theta = deltaTheta * r;
			float phi = deltaPhi * s;

			mVertices[index] = sin(theta) * cos(phi);
			mVertices[index + 1] = sin(theta) * sin(phi);
			mVertices[index + 2] = cos(theta);

			index += 3;
		}
	}
}

void MyLandscape::calculateIndices(unsigned int segments, unsigned int rings)
{
	unsigned int nTriangles = segments * (1 + 2 * (rings - 1));

	mIndices = std::vector<GLushort>(3 * nTriangles);
	for (unsigned int i = 0; i < segments; i++)
	{
		unsigned int index = i * 3;

		mIndices[index] = -1;
		mIndices[index + 1] = ((i + 1) * rings) % (rings * segments);
		mIndices[index + 2] = i * rings;
	}

	for (unsigned int i = 0; i < segments; i++)
	{
		unsigned int index = 3 * segments + i * 6;

		mIndices[index] = i * rings;
		mIndices[index + 1] = ((i + 1) * rings) % (rings * segments);
		mIndices[index + 2] = i * rings + 1;

		mIndices[index + 3] = i * rings + 1;
		mIndices[index + 4] = ((i + 1) * rings) % (rings * segments);
		mIndices[index + 5] = ((i + 1) * rings + 1) % (rings * segments);
	}

	for (unsigned int i = 0; i < 2 * segments * (rings - 2) * 3; i++)
		mIndices[9 * segments + i] = mIndices[3 * segments + i] + 1;

	for (unsigned int i = 0; i < mIndices.size(); i++)
		mIndices[i]++;
}

void MyLandscape::calculateNormals()
{
	mNormals = std::vector<GLfloat>(mVertices.size());

	for (unsigned int i = 0; i < mNormals.size(); i++)
		mNormals[i] = -mVertices[i];
}

void MyLandscape::calculateTextels()
{
	mTextels = std::vector<GLfloat>(2 * mVertices.size() / 3);

	for (unsigned int i = 0; i < mTextels.size() / 2; i++)
	{
		unsigned int verticesI = i * 3;
		unsigned int textelsI = i * 2;

		mTextels[textelsI] = (mVertices[verticesI] + 1) / 2.0f;
		mTextels[textelsI + 1] = (mVertices[verticesI + 1] + 1) / 2.0f;
	}
}